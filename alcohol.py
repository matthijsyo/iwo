#!/usr/bin/python3
# File name: alcohol.py
# Description: 
# Author: M. van Hofslot
# Date: 26 - 3 - 2018

import sys    

def main():
    alc_tweets = []
    for line in sys.stdin:
        tweet = line.rstrip().split("\t")
        drinks = [drink.strip() for drink in open('drinks.txt').readlines()]
        for drink in drinks:
            if " " + drink + " " in tweet[0].lower():
                alc_tweets.append(tweet)
    print(alc_tweets)

if __name__ == "__main__":
    main()
