#!/usr/bin/python3
# File name: data.py
# Description: 
# Author: M. van Hofslot
# Date: 11 - 6 - 2018

import sys

def main():
    i = 0
    e = 0
    for f in range(0,21):
        s = str(f)
        if len(s) == 1:
            s = "0" + s
        with open(s+'.txt') as data:
            for line in data:
                if f % 7 in range(0,4):
                    i += 1
                else:
                    e += 1
       
        f += 1
    print(i, e)
if __name__ == "__main__":
    main()
