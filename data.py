#!/usr/bin/python3
# File name: data.py
# Description: 
# Author: M. van Hofslot
# Date: 11 - 6 - 2018

import sys
from ast import literal_eval

def main():
    tweets = []
    with open('result.txt', 'rt') as f:
        for line in f:
            day = literal_eval(line)
            tweets.append(day)
    i = 0
    total = 0
    days = {'mon': [], 'tue': [], 'wed': [], 'thu': [], 'fri': [], 'sat': [], 'sun': []}
    for day in tweets:
        total += len(day)
        if i % 7 == 0:
            days['mon'].append(len(day))
        elif i % 7 == 1:
            days['tue'].append(len(day))
        elif i % 7 == 2:
            days['wed'].append(len(day))
        elif i % 7 == 3:
            days['thu'].append(len(day))
        elif i % 7 == 4:
            days['fri'].append(len(day))
        elif i % 7 == 5:
            days['sat'].append(len(day))
        else:
            days['sun'].append(len(day))
        i += 1
    print(days,"\nTotal: {0}".format(total))
if __name__ == "__main__":
    main()
